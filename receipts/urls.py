from django.urls import path
from receipts.views import (
    show_receipt,
    create_receipt,
    expense_categories,
    accounts,
    create_expense_category,
    create_account
    )


urlpatterns = [
    path("", show_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_categories,
         name="category_list"),
    path("accounts/", accounts, name="account_list"),
    path("categories/create/", create_expense_category,
         name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
